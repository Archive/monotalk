SUBDIRS=ObjectBrowser

all:
	@for dir in $(SUBDIRS); do \
		$(MAKE) -C $$dir $(MFLAGS); \
	done
