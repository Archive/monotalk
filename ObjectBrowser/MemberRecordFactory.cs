using System;
using System.Collections;
using System.Reflection;

using GLib;
using Gdk;

namespace MonoTalk
{
	public abstract class MemberRecordFactory
	{
		public MemberRecord [] Info;
		protected BindingFlags flags;
		protected Type type;

		public MemberRecordFactory (BindingFlags flags)
		{
			this.flags = flags;
		}

		public virtual BindingFlags Flags
		{
			set {
				bool changed = flags != value;
				flags = value;
				if (changed)
					Load ();
			}
			get {
				return flags;
			}
		}

		public virtual Type Type {
			set {
				bool changed = type != value;
				type = value;
				if (changed)
					Load ();
			}
		}

		public virtual bool HideDuplicates {
			get {
				return true;
			}
		}

		public string FullTitle
		{
			get {
				if (Info == null || Info.Length == 0)
					return Title;
				else
					return "<b>" + Title + " (" + Info.Length + ")</b>";
			}
		}

		public virtual void Load ()
		{
			MemberInfo[] fields = Members;
			Info = new MemberRecord [fields.Length];
			int count = 0;

			foreach (MemberInfo mi in fields) {
				Info [count] = Record (mi);
				count ++;
			}
		}

		protected abstract MemberRecord Record (MemberInfo mi);
		protected abstract MemberInfo[] Members { get; }
		public abstract string Title { get; }
		public abstract string ColumnTitle { get; }
	}

	public class MethodRecordFactory : MemberRecordFactory
	{
		public MethodRecordFactory (BindingFlags flags) : base (flags)
		{
		}

		public override string Title {
			get {
				return "methods";
			}
		}

		public override string ColumnTitle {
			get {
				return "Method";
			}
		}

		public override void Load ()
		{
			MethodInfo[] methods = (MethodInfo[]) Members;
			int count = 0;

			foreach (MethodInfo mi in methods)
			        if (!mi.IsSpecialName)
					count ++;

			Info = new MethodRecord [count];
			count = 0;
			foreach (MethodInfo mi in methods)
			        if (!mi.IsSpecialName) {
					Info [count] = Record (mi);
					count ++;
				}
		}

		protected override MemberRecord Record (MemberInfo mi)
		{
			return new MethodRecord ((MethodInfo) mi);
		}

		protected override MemberInfo[] Members {
			get {
				return type.GetMethods (flags);
			}
		}
	}

	public class ConstructorRecordFactory : MemberRecordFactory
	{
		public ConstructorRecordFactory (BindingFlags flags) : base (flags);

		public override string Title {
			get {
				return "constructors";
			}
		}

		public override string ColumnTitle {
			get {
				return "Constructor";
			}
		}

		public override bool HideDuplicates {
			get {
				return false;
			}
		}

		protected override MemberRecord Record (MemberInfo mi)
		{
			return new ConstructorRecord ((ConstructorInfo) mi);
		}

		protected override MemberInfo[] Members {
			get {
				return type.GetConstructors (flags);
			}
		}
	}

	public class EventRecordFactory : MemberRecordFactory
	{
		public EventRecordFactory (BindingFlags flags) : base (flags);

		public override string Title {
			get {
				return "events";
			}
		}

		public override string ColumnTitle {
			get {
				return "Event";
			}
		}

		protected override MemberRecord Record (MemberInfo mi)
		{
			return new EventRecord ((EventInfo) mi);
		}

		protected override MemberInfo[] Members {
			get {
				return type.GetEvents (flags);
			}
		}
	}

	public class FieldRecordFactory : MemberRecordFactory
	{
		public FieldRecordFactory (BindingFlags flags) : base (flags)
		{
		}

		public override string Title {
			get {
				return "fields";
			}
		}

		public override string ColumnTitle {
			get {
				return "Field";
			}
		}

		protected override MemberRecord Record (MemberInfo mi)
		{
			return new FieldRecord ((FieldInfo) mi);
		}

		protected override MemberInfo[] Members {
			get {
				return type.GetFields (flags);
			}
		}
	}

	public class PropertyRecordFactory : MemberRecordFactory
	{
		public PropertyRecordFactory (BindingFlags flags) : base (flags);

		public override string Title {
			get {
				return "properties";
			}
		}

		public override string ColumnTitle {
			get {
				return "Property";
			}
		}

		protected override MemberRecord Record (MemberInfo mi)
		{
			return new PropertyRecord ((PropertyInfo) mi);
		}

		protected override MemberInfo[] Members {
			get {
				return type.GetProperties (flags);
			}
		}
	}

	public class AllRecordFactory : MemberRecordFactory
	{
		private ArrayList list;

		public AllRecordFactory (BindingFlags flags) : base (flags)
		{
			list = new ArrayList ();
		}

		public void Add (MemberRecordFactory view)
		{
			list.Add (view);
		}

		public override Type Type {
			set {
				bool changed = type != value;
				type = value;
				foreach (MemberRecordFactory view in list)
				        view.Type = value;
				if (changed)
					Load ();
			}
		}

		public override string Title {
			get {
				return "all";
			}
		}

		public override string ColumnTitle {
			get {
				return "Member";
			}
		}

		public override void Load ()
		{
			ArrayList infoList = new ArrayList ();
			int count = 0;

			foreach (MemberRecordFactory view in list) {
				count += view.Info.Length;
				infoList.AddRange (view.Info);
			}

			Info = (MemberRecord[]) infoList.ToArray (typeof (MemberRecord));
		}

		protected override MemberRecord Record (MemberInfo mi)
		{
			return new MethodRecord ((MethodInfo) mi);
		}

		protected override MemberInfo[] Members {
			get {
				return type.GetMethods (flags);
			}
		}
	}
}
