using System;
using System.AppDomain;
using System.Drawing;
using System.Reflection;
using Gtk;
using Gdk;
using GtkSharp;
using Gnome;
using MonoTalk;

public class DemoApp : Gnome.App
{
	ObjectBrowser browser;
	private static readonly int versionMajor = 0;
	private static readonly int versionMinor = 1;
	private static readonly string version = versionMajor + "." + versionMinor;

	private void WindowDelete (object obj, DeleteEventArgs args)
	{
		Application.Quit ();
		args.RetVal = true;
	}
		
	private void QuitCallback (object o, EventArgs args)
	{
		Application.Quit ();
	}
		
	private void AboutCallback (object o, EventArgs args)
	{
		Pixbuf logo = new Pixbuf ("pixmaps" + System.IO.Path.DirectorySeparatorChar + "kapradina.png");
		String[] authors = new string[] {
			"Radek Doulik (rodo@matfyz.cz)"
		};
		string[] documentors = new string[] {};

		About about = new About ("Object Browser", version,
					 "Copyright (C) 2002 Radek Doulik",
					 "A demo application of ObjectBrowser class",
					 authors, documentors, "", logo);
		about.Show ();
	}

	private Gtk.MenuBar CreateMenus ()
	{
		AccelGroup group = new AccelGroup ();
		MenuBar menuBar = new MenuBar ();
			
		Menu file = new Menu ();
		MenuItem item = new MenuItem ("_File");
		item.Submenu = file;
			
		ImageMenuItem quit = new ImageMenuItem (Gtk.Stock.Quit, group);
		quit.Activated += new EventHandler (QuitCallback);
		file.Append (quit);
		menuBar.Append (item);

		Menu help = new Menu ();
		item = new ImageMenuItem (Gtk.Stock.Help, group);
		item.Submenu = help;
		item.RightJustified = true;
			
		ImageMenuItem about = new ImageMenuItem (Gnome.Stock.About, group);
		about.Activated += new EventHandler (AboutCallback);
		help.Append (about);
		menuBar.Append (item);
		menuBar.ShowAll ();

		return menuBar;
	}

	private DemoApp (string[] args) : base ("ObjectBrowserDemo", "Object Browser Demo")
	{
		DeleteEvent += new DeleteEventHandler (WindowDelete);

		Menus = CreateMenus ();
		browser = new ObjectBrowser ();
		//browser.LoadAssembly (Assembly.GetAssembly (System.Type.GetType ("System.Object")));
		if (args.Length > 0) {
			browser.LoadAssembly (AppDomain.CurrentDomain.Load (args [0]));
		} else
			browser.LoadAssembly (Assembly.GetAssembly (System.Type.GetType ("DemoApp")));
		Contents = browser;

		DefaultSize = new Drawing.Size (720, 540);
	}

	public static int Main (string[] args)
	{
		Program program = new Program ("ObjectBrowserDemo", version, Modules.UI, args);

		new DemoApp (args).ShowAll ();
		program.Run ();

		return 0;
	}
}
