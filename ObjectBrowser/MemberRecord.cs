using System;
using System.Reflection;

using GLib;
using Gdk;

namespace MonoTalk
{
	public abstract class MemberRecord
	{
		private static readonly Pixbuf icon = new Pixbuf (null, "transparent.png");

		protected MemberInfo mi;

		public MemberRecord (MemberInfo info)
		{
			mi = info;
		}

		public abstract string Label {
			get;
		}

		public virtual Pixbuf Icon  {
			get {
				return icon;
			}
		}

		public virtual string Name {
			get {
				return mi.Name;
			}
		}

		protected string MethodLabel (string name)
		{
			ParameterInfo[] pi = ((MethodBase) mi).GetParameters ();
			string parms = "";

			for (int i = 0; i < pi.Length; i ++) {
				if (i > 0)
					parms = parms + ", ";
				parms += "<span color=\"blue\">" + pi [i].ParameterType.Name + "</span>";
			}
			return "<b>" + name + "</b> (" + parms + ")";
		}
	}

	public class MethodRecord : MemberRecord
	{
		private static readonly new Pixbuf icon = new Pixbuf (null, "method.png");

		public override string Label {
			get {
				return MethodLabel (mi.Name);
			}
		}

		public override Pixbuf Icon {
			get {
				return icon;
			}
		}

		public MethodRecord (MethodInfo mi) : base (mi);
	}

	public class ConstructorRecord : MemberRecord
	{
		private static readonly new Pixbuf icon = new Pixbuf (null, "constructor.png");

		public ConstructorRecord (ConstructorInfo mi) : base (mi);

		public override string Label {
			get {
				return MethodLabel (mi.DeclaringType.Name);
			}
		}

		public override Pixbuf Icon {
			get {
				return icon;
			}
		}

		public override string Name {
			get {
				return mi.DeclaringType.Name;
			}
		}
	}

	public class EventRecord : MemberRecord
	{
		private static readonly new Pixbuf icon = new Pixbuf (null, "method.png");

		public EventRecord (EventInfo mi) : base (mi);

		public override string Label {
			get {
				return MethodLabel (mi.Name);
			}
		}

		public override Pixbuf Icon {
			get {
				return icon;
			}
		}
	}

	public class FieldRecord : MemberRecord
	{
		private static readonly new Pixbuf icon = new Pixbuf (null, "field.png");

		public override Pixbuf Icon {
			get {
				return icon;
			}
		}

		public override string Label {
			get {
				return "<b>" + mi.Name + "</b>, <span color=\"blue\">" + ((FieldInfo) mi).FieldType.Name + "</span>";
			}
		}

		public FieldRecord (FieldInfo mi) : base (mi);
	}

	public class PropertyRecord : MemberRecord
	{
		private static readonly Pixbuf iconRO = new Pixbuf (null, "prop-read-only.png");
		private static readonly Pixbuf iconWO = new Pixbuf (null, "prop-write-only.png");
		private static readonly Pixbuf iconRW = new Pixbuf (null, "prop-read-write.png");

		public PropertyRecord (PropertyInfo mi) : base (mi);

		public override Pixbuf Icon {
			get {
				PropertyInfo pi = (PropertyInfo) mi;

				if (pi.CanRead) {
					if (pi.CanWrite)
						return iconRW;
					return iconRO;
				}
				return iconWO;
			}
		}

		public override string Label {
			get {
				return "<b>" + mi.Name + "</b>, <span color=\"blue\">" + ((PropertyInfo) mi).PropertyType.Name + "</span>";
			}
		}
	}
}
