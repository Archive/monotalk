namespace MonoTalk {

	using System;
	using System.Collections;
	using System.Drawing;
	using System.Reflection;

	using GLib;
	using Gdk;
	using Gtk;
	using GtkSharp;

        public enum MemberFilter {
		AllMembers = 0,
		Fields,
		Properties,
		Methods,
		Constructors,
		Events,
	}

	public class ObjectBrowser : VPaned {
                private HBox hbox;
		private VBox vbox;
                private TreeStore typeStore;
		private ListStore memberStore;
		private TreeView typeView;
                private MemberList memberView;
                private TextView text;
		private TextBuffer buffer;
		private BindingFlags flags;
		private MemberRecordFactory[] recordFactory;
		private bool assemblyOnly = true;
		private Assembly assembly;

		public ObjectBrowser () : base ()
		{
			AllRecordFactory all;
			hbox = new HBox (true, 2);

			flags = BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance;
			recordFactory = new MemberRecordFactory [6];
			recordFactory [0] = all = new AllRecordFactory (flags);
			recordFactory [1] = new FieldRecordFactory (flags);
			recordFactory [2] = new PropertyRecordFactory (flags);
			recordFactory [3] = new MethodRecordFactory (flags);
			recordFactory [4] = new ConstructorRecordFactory (flags);
			recordFactory [5] = new EventRecordFactory (flags);
			all.Add (recordFactory [1]);
			all.Add (recordFactory [2]);
			all.Add (recordFactory [3]);
			all.Add (recordFactory [4]);
			all.Add (recordFactory [5]);

			typeStore = new TreeStore ((int) TypeFundamentals.TypeString, (int) TypeFundamentals.TypeString);
			typeView = new TreeView (typeStore);
			TreeViewColumn col = new TreeViewColumn ();
			CellRenderer renderer = new CellRendererText ();
			col.Title = "Class";
			col.PackStart (renderer, true);
			col.AddAttribute (renderer, "text", 0);
			typeView.AppendColumn (col);
			typeView.Selection.Changed += new EventHandler (TypeSelectionChanged);
			typeStore.SetSortColumnId (0, SortType.Ascending);

			ScrolledWindow sw = new ScrolledWindow ();
			sw.Add (typeView);
			hbox.Add (sw);
			
			vbox = new VBox (true, 2);
			vbox.Add (MemberSelector ());
			vbox.Add (ICSelector ());
			hbox.Add (vbox);

			memberView = new MemberList ();
			memberView.RecordFactory = recordFactory [0];

			sw = new ScrolledWindow ();
			sw.Add (memberView);
			hbox.Add (sw);

			buffer = new TextBuffer (new TextTagTable ());
			buffer.SetText ("\n\n\tThis TextView should contain member source code (when available) in the future.");
			text = new TextView (buffer);

			Add1 (hbox);
			Add2 (text);

			Position = 300;
		}

		private BindingFlags Flags {
			set {
				//Console.WriteLine ("flags {0} --> {1}", flags, value);
				flags = value;
				foreach (MemberRecordFactory rv in recordFactory) {
					rv.Flags = flags;
				}
				RefreshMemberSelector ();
				memberView.Refresh ();
			}
		}

		private Type Type {
			set {
				TreeIter mIter;

				memberStore.IterChildren (out mIter);
				foreach (MemberRecordFactory rv in recordFactory) {
					rv.Type = value;
					memberStore.IterNext (out mIter);
				}
				RefreshMemberSelector ();
				memberView.Refresh ();
			}
		}

		private int Row (TreeIter iter)
		{
			return Convert.ToInt32 (memberStore.GetPath (iter).ToString ());
		}

		private void MemberFilterSelectionChanged (object o, EventArgs args)
		{
			TreeSelection selection = (TreeSelection) o;
			TreeModel model;
			TreeIter iter = new TreeIter ();

			//Console.WriteLine ("changed {0}", selection);
			if (selection != null) {
				selection.GetSelected (out model, ref iter);
				if (model != null) {
					memberView.RecordFactory = recordFactory [Row (iter)];
				}
			}
		}

		private void RefreshMemberSelector ()
		{
			TreeIter iter;

			memberStore.IterChildren (out iter);
			foreach (MemberRecordFactory rv in recordFactory) {
				memberStore.SetValue (iter, 0, new GLib.Value (rv.FullTitle));
				memberStore.IterNext (out iter);
			}
		}

		private Widget MemberSelector ()
		{
			ScrolledWindow sw = new ScrolledWindow ();
			memberStore = new ListStore ((int) TypeFundamentals.TypeString, (int) TypeFundamentals.TypeInt);
			TreeView view = new TreeView (memberStore);
			TreeViewColumn col = new TreeViewColumn ();
			TreeIter iter;
			CellRenderer renderer = new CellRendererText ();

			col.Title = "MemberType";
			col.PackStart (renderer, true);
			col.AddAttribute (renderer, "markup", 0);
			view.AppendColumn (col);
			view.HeadersVisible = false;

			foreach (MemberRecordFactory rv in recordFactory) {
				memberStore.Append (out iter);
				memberStore.SetValue (iter, 0, new GLib.Value (rv.FullTitle));
			}

			view.Selection.SelectPath (new TreePath ("0"));
			view.Selection.Changed += new EventHandler (MemberFilterSelectionChanged);

			sw.Add (view);

			return sw;
		}

		private void ICSelectionChanged (object o, EventArgs args)
		{
			TreeSelection selection = (TreeSelection) o;
			TreeModel model;
			TreeIter iter = new TreeIter ();
			GLib.Value val = new GLib.Value ();

			//Console.WriteLine ("changed {0}", selection);
			selection.GetSelected (out model, ref iter);
			model.GetValue (iter, 1, val);

			Flags = (flags & ~(BindingFlags.Instance | BindingFlags.Static)) | (BindingFlags) ((int) val);
		}

		private Widget ICSelector ()
		{
			ScrolledWindow sw = new ScrolledWindow ();
			ListStore store = new ListStore ((int) TypeFundamentals.TypeString, (int) TypeFundamentals.TypeInt);
			TreeView view = new TreeView (store);
			TreeViewColumn col = new TreeViewColumn ();
			TreeIter iter;
			CellRenderer renderer = new CellRendererText ();

			col.Title = "MemberType";
			col.PackStart (renderer, true);
			col.AddAttribute (renderer, "text", 0);
			view.AppendColumn (col);
			view.HeadersVisible = false;

			store.Append (out iter);
			store.SetValue (iter, 0, new GLib.Value ("all"));
			store.SetValue (iter, 1, new GLib.Value ((int) (BindingFlags.Static | BindingFlags.Instance)));
			store.Append (out iter);
			store.SetValue (iter, 0, new GLib.Value ("instance"));
			store.SetValue (iter, 1, new GLib.Value ((int) BindingFlags.Instance));
			store.Append (out iter);
			store.SetValue (iter, 0, new GLib.Value ("class"));
			store.SetValue (iter, 1, new GLib.Value ((int) BindingFlags.Static));

			view.Selection.SelectPath (new TreePath ("0"));
			view.Selection.Changed += new EventHandler (ICSelectionChanged);

			sw.Add (view);
			return sw;
		}

		private void SetCount (string label, int row, MemberInfo[] arr)
		{
			Console.WriteLine ("changed {0} {1}", row, row.ToString ());
			TreeIter iter;
			if (memberStore.GetIterFromString (out iter, row.ToString ())) {
				int len = arr.Length;
				memberStore.SetValue (iter, 0, new GLib.Value (len == 0 ? label : label + " (" + len + ")"));
			}
		}

		private void TypeSelectionChanged (object o, EventArgs args)
		{
			TreeSelection selection = (TreeSelection) o;
			TreeModel model;
			TreeIter iter = new TreeIter ();
			GLib.Value val = new GLib.Value ();

			//Console.WriteLine ("changed {0}", selection);
			selection.GetSelected (out model, ref iter);
			model.GetValue (iter, 1, val);

			Type = System.Type.GetType ((string) val);
		}

		object AddType (Type t, Hashtable ht)
		{
			TreeIter iter;

			//Console.WriteLine ("add type {0}", t.FullName);

			if (!t.IsClass)
				return null;

			//Console.WriteLine ("try type {0}", t.FullName);
			if (ht [t] != null)
				return (TreeIter) ht [t];

			//Console.WriteLine ("new line {0}", t.FullName);

			iter = new TreeIter ();
			if (t.BaseType == null || t.BaseType.Assembly != assembly)
				typeStore.Append (out iter);
			else
				typeStore.Append (out iter, (TreeIter) AddType (t.BaseType, ht));

			typeStore.SetValue (iter, 0, new GLib.Value (t.Name));
			typeStore.SetValue (iter, 1, new GLib.Value (t.FullName));

			ht [t] = iter;

			return iter;
		}

                public void LoadAssembly (Assembly asm)
		{
			Hashtable ht = new Hashtable ();
			assembly = asm;

			//Console.WriteLine ("CreateTree {0} {1}", asm, ht);
			foreach (Type t in asm.GetTypes ()) {
				//Console.WriteLine ("calling AddType ({0})", t.FullName);
				AddType (t, ht);
			}
			typeView.ExpandRow (new TreePath ("0"), false);
		}
	}

        public class MemberList : TreeView {
                private TreeStore store;
		private MemberRecordFactory view;
		private TreeViewColumn column;

		public MemberRecordFactory RecordFactory {
			set {
				view = value;
				column.Title = view.ColumnTitle;
				Refresh ();
			}
		}

		public void Refresh () {
			store.Clear ();
			if (view != null && view.Info != null) {
				Hashtable count = new Hashtable ();
				Hashtable parent = new Hashtable ();
				TreeIter iter;

				foreach (MemberRecord mr in view.Info) {
					if (view.HideDuplicates && count [mr.Name] != null) {
						if ((int) count [mr.Name] == 1) {
							TreeIter orig = (TreeIter) parent [mr.Name];
							TreeIter root;
							GLib.Value name = new GLib.Value ();
							GLib.Value value = new GLib.Value ();
							GLib.Value image = new GLib.Value ();

							store.GetValue (orig, 0, name);
							store.GetValue (orig, 1, value);
							store.GetValue (orig, 2, image);
							store.Remove (out orig);

							store.Append (out root);
							store.SetValue (root, 0, new GLib.Value (mr.Name));
							store.SetValue (root, 1, new GLib.Value (mr.Name + " (2)"));

							parent [mr.Name] = root;

							store.Append (out orig, root);
							store.SetValue (orig, 0, name);
							store.SetValue (orig, 1, value);
							store.SetValue (orig, 2, image);

							store.Append (out iter, root);
						} else {
							store.Append (out iter, (TreeIter) parent [mr.Name]);
							store.SetValue ((TreeIter) parent [mr.Name], 0, new GLib.Value (mr.Name));
							store.SetValue ((TreeIter) parent [mr.Name], 1, new GLib.Value (mr.Name
															+ " (" + ((int) count [mr.Name] + 1) + ")"));
						}
						
						count [mr.Name] = 1 + (int) count [mr.Name];

					} else {
						store.Append (out iter);
						count [mr.Name] = 1;
						parent [mr.Name] = iter;
					}

					store.SetValue (iter, 0, new GLib.Value (mr.Name));
					store.SetValue (iter, 1, new GLib.Value (mr.Label));
					store.SetValue (iter, 2, new GLib.Value (mr.Icon));
				}
			}
		}

                public MemberList () : base ()
		{
			store = new TreeStore ((int) TypeFundamentals.TypeString, (int) TypeFundamentals.TypeString, Gdk.Pixbuf.GType);
			store.SetSortColumnId (0, SortType.Ascending);

			column = new TreeViewColumn ();
			CellRenderer image = new CellRendererPixbuf ();
			CellRenderer text = new CellRendererText ();
			column.Title = "Member";
			column.PackStart (image, false);
			column.AddAttribute (image, "pixbuf", 2);
			column.PackStart (text, true);
			column.AddAttribute (text, "markup", 1);

			AppendColumn (column);
			Selection.Changed += new EventHandler (MemberSelectionChanged);

			Model = store;
		}

                private static void MemberSelectionChanged (object o, EventArgs args)
		{
			TreeSelection selection = (TreeSelection) o;
			TreeModel model;
			TreeIter iter = new TreeIter ();
			GLib.Value val = new GLib.Value ();

			selection.GetSelected (out model, ref iter);
			model.GetValue (iter, 0, val);
		}
	}
}
